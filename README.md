## aosp_blossom-userdebug 13 TQ2A.230505.002.A1 1686840307 release-keys
- Manufacturer: xiaomi
- Platform: mt6765
- Codename: blossom
- Brand: Redmi
- Flavor: aosp_blossom-userdebug
- Release Version: 13
- Kernel Version: 4.19.127
- Id: TQ2A.230505.002.A1
- Incremental: 1686840307
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Redmi/aosp_blossom/blossom:13/TQ2A.230505.002.A1/root06152015:userdebug/release-keys
- OTA version: 
- Branch: aosp_blossom-userdebug-13-TQ2A.230505.002.A1-1686840307-release-keys
- Repo: redmi_blossom_dump_13703
